import os

###########################################################################
## Handout painting code.
###########################################################################
from PIL import Image
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
import time
import matplotlib.image as mpimg
import scipy as sci
import canny as cn
from math import hypot
from scipy.signal import convolve2d as conv

np.set_printoptions(threshold = np.nan)  

def colorImSave(filename, array):
    imArray = sci.misc.imresize(array, 3., 'nearest')
    if (len(imArray.shape) == 2):
        sci.misc.imsave(filename, cm.jet(imArray))
    else:
        sci.misc.imsave(filename, imArray)

def markStroke(mrkd, p0, p1, rad, val):
    # Mark the pixels that will be painted by
    # a stroke from pixel p0 = (x0, y0) to pixel p1 = (x1, y1).
    # These pixels are set to val in the ny x nx double array mrkd.
    # The paintbrush is circular with radius rad>0
    
    sizeIm = mrkd.shape
    sizeIm = sizeIm[0:2];
    nx = sizeIm[1]
    ny = sizeIm[0]
    p0 = p0.flatten('F')
    p1 = p1.flatten('F')
    rad = max(rad,1)
    # Bounding box
    concat = np.vstack([p0,p1])
    bb0 = np.floor(np.amin(concat, axis=0))-rad
    bb1 = np.ceil(np.amax(concat, axis=0))+rad
    # Check for intersection of bounding box with image.
    intersect = 1
    if (bb0[0] > nx) or (bb0[1] > ny) or (bb1[0] < 1) or (bb1[1] < 1):
        intersect = 0
    if intersect:
        # Crop bounding box.
        bb0 = np.amax(np.vstack([np.array([bb0[0], 1]), np.array([bb0[1],1])]), axis=1)
        bb0 = np.amin(np.vstack([np.array([bb0[0], nx]), np.array([bb0[1],ny])]), axis=1)
        bb1 = np.amax(np.vstack([np.array([bb1[0], 1]), np.array([bb1[1],1])]), axis=1)
        bb1 = np.amin(np.vstack([np.array([bb1[0], nx]), np.array([bb1[1],ny])]), axis=1)
        # Compute distance d(j,i) to segment in bounding box
        tmp = bb1 - bb0 + 1
        szBB = [tmp[1], tmp[0]]
        q0 = p0 - bb0 + 1
        q1 = p1 - bb0 + 1
        t = q1 - q0
        nrmt = np.linalg.norm(t)
        [x,y] = np.meshgrid(np.array([i+1 for i in range(int(szBB[1]))]), np.array([i+1 for i in range(int(szBB[0]))]))
        d = np.zeros(szBB)
        d.fill(float("inf"))
        
        if nrmt == 0:
            # Use distance to point q0
            d = np.sqrt( (x - q0[0])**2 +(y - q0[1])**2)
            idx = (d <= rad)
        else:
            # Use distance to segment q0, q1
            t = t/nrmt
            n = [t[1], -t[0]]
            tmp = t[0] * (x - q0[0]) + t[1] * (y - q0[1])
            idx = (tmp >= 0) & (tmp <= nrmt)
            if np.any(idx.flatten('F')):
                d[np.where(idx)] = abs(n[0] * (x[np.where(idx)] - q0[0]) + n[1] * (y[np.where(idx)] - q0[1]))
            idx = (tmp < 0)
            if np.any(idx.flatten('F')):
                d[np.where(idx)] = np.sqrt( (x[np.where(idx)] - q0[0])**2 +(y[np.where(idx)] - q0[1])**2)
            idx = (tmp > nrmt)
            if np.any(idx.flatten('F')):
                d[np.where(idx)] = np.sqrt( (x[np.where(idx)] - q1[0])**2 +(y[np.where(idx)] - q1[1])**2)

            # Pixels within crop box to paint have distance <= rad
            idx = (d <= rad)

        # Marks the pixels
        if np.any(idx.flatten('F')):
            xy = (bb0[1]-1+y[np.where(idx)] + sizeIm[0] * (bb0[0]+x[np.where(idx)]-2)).astype(int)
            sz = mrkd.shape
            m = mrkd.flatten('F')
            m[xy - 1] = val
            mrkd = m.reshape(mrkd.shape[0], mrkd.shape[1], order = 'F')

            '''
            row = 0
            col = 0
            for i in range(len(m)):
                col = i//sz[0]
                mrkd[row][col] = m[i]
                row += 1
                if row >= sz[0]:
                    row = 0
            '''
            
            
            
    return mrkd

def paintStroke(canvas, x, y, p0, p1, colour, rad):
    # Paint a stroke from pixel p0 = (x0, y0) to pixel p1 = (x1, y1)
    # on the canvas (ny x nx x 3 double array).
    # The stroke has rgb values given by colour (a 3 x 1 vector, with
    # values in [0, 1].  The paintbrush is circular with radius rad>0
    sizeIm = canvas.shape
    sizeIm = sizeIm[0:2]
    idx = markStroke(np.zeros(sizeIm), p0, p1, rad, 1) > 0
    # Paint
    if np.any(idx.flatten('F')):
        canvas = np.reshape(canvas, (np.prod(sizeIm),3), "F")
        xy = y[idx] + sizeIm[0] * (x[idx]-1)
        canvas[xy-1, :] = np.tile(np.transpose(colour[:]), (len(xy), 1))
        canvas = np.reshape(canvas, sizeIm + (3,), "F")
    return canvas


def intensityImg(img):
    """
    Returns Litwinowiczs' intensity image
    """
    r = img[:, :, 0]
    g = img[:, :, 1]
    b = img[:, :, 2]

    return 0.3 * r + 0.59 * g + 0.11 * b
    # return 0.3 * r        # used for part 3

def inCanvas(canny_img, x, y):
    """
    Returns true if (x, y) is within the boundaries of "canny_img"
    """
    shape = canny_img.shape

    if x >= shape[0]:
        return False

    if y >= shape[1]:
        return False

    if x < 0 or y < 0:
        return False    # avoids list reverse access

    return True


def stoke_clip(canny_img, cntr, delta, halflen):
    """
    Finds the endpoints for the given stroke center and direction.
    """
    xk = np.copy(cntr)

    # Determines if the coordinates are valid
    if not inCanvas(canny_img, xk[1], xk[0]):
            return False, xk

    # Determines if the center is an edgel
    if canny_img[cntr[1], cntr[0]] == 1:
            # Center is the edgel
            return True, cntr

    # Determines the space vector
    s = np.array([0, 0])
    if abs(delta[0]) >= abs(delta[1]):
        s = delta/abs(float(delta[0]))
    else:
        s = delta/abs(float(delta[1]))

    k = 1
    while True:

        # Calculates the distance for this iteration
        dist = np.round(k * s)

        # Calculates the potential end point
        xk_t = cntr + dist

        diff = xk_t - cntr
        magn = hypot(diff[1], diff[0])
        if (magn > float(halflen)):
            # reached the maximum distance
            return False, xk

        # Ensures the new coordinate is valid
        if not inCanvas(canny_img, xk_t[1], xk_t[0]):
            return False, xk

        # Determine if an edge is detected
        if canny_img[xk_t[1], xk_t[0]] == 1:
            return False, xk

        xk = np.copy(xk_t)

        k += 1


def generateTheta(im, threshold=10):
    """
    Generates a gradient and theta matrices for the supplied image intensity
    matrix.
    Applies gaussian filter first to pre-compute a smooth image of gradient
    vectors. Returns a tuple containing the gradient and theta matrices.
    """
    imin = im.copy() * 255.0

    # Creates a gaussian kernel with sigma = 45 and window = 3
    gausskernel = cn.gaussFilter(45, window = 3)

    # Creates horizontal and vertical filters
    fx = cn.createFilter([0,  1, 0,
                          0,  0, 0,
                          0, -1, 0])
    fy = cn. createFilter([0, 0, 0,
                           1, 0, -1,
                           0, 0, 0])

    # Applies the gaussian filter
    imout = conv(imin, gausskernel, 'valid')

    gradxx = conv(imout, fx, 'valid')
    gradyy = conv(imout, fy, 'valid')

    gradx = np.zeros(im.shape)
    grady = np.zeros(im.shape)
    padx = (imin.shape[0] - gradxx.shape[0]) / 2.0
    pady = (imin.shape[1] - gradxx.shape[1]) / 2.0
    gradx[padx:-padx, pady:-pady] = gradxx
    grady[padx:-padx, pady:-pady] = gradyy

    # Net gradient is the square root of sum of square of the horizontal
    # and vertical gradients

    grad = np.hypot(gradx, grady)
    theta = arctan2(grady, gradx)
    # theta = 180 + (180 / pi) * theta

    # Gradients lower then the threshold are removed
    xx, yy = where(grad < threshold)
    theta[xx, yy] = pi/12
    grad[xx, yy] = 0


    colorImSave("theta.png", theta)

    return grad, theta


def doPart(partNum, imgLoc):
    print "Performing part #: ", partNum

    # Threshold values
    tLow = tHigh = 0

    if partNum <= 2:
        tLow = 10
        tHigh = 85

    elif partNum == 3:
        tLow = 1000
        tHigh = 1200
    else:
        tLow = 5
        tHigh = 175

    # Reads the image
    imRGB = array(Image.open(imgLoc))

    # Derives an intensity image
    imgI = intensityImg(imRGB)

    # Computes the canny edgels of single monochrome image
    canny_img = cn.canny(imgI, 2.0, tHigh, tLow)

    # Convert the image to double, and scale each R,G,B channel to range [0,1].
    imRGB = double(imRGB) / 255.0

    # Generates theta angle matrix
    grad, theta = generateTheta(imgI, tLow)

    plt.clf()
    plt.axis('off')

    sizeIm = imRGB.shape
    sizeIm = sizeIm[0:2]

    # Sets radius of paint brush and half length of drawn lines
    rad = 1
    halfLen = 3

    # Set up x, y coordinate images, and canvas.
    [x, y] = np.meshgrid(np.array([i+1 for i in range(int(sizeIm[1]))]),
                         np.array([i+1 for i in range(int(sizeIm[0]))]))

    canvas = np.zeros((sizeIm[0], sizeIm[1], 3))

    # Initially mark the canvas with a value out of range.
    # Negative values will be used to denote pixels which are unpainted.
    canvas.fill(-1)

    # Random number seed
    np.random.seed(29645)

    delta = np.array([])

    if partNum < 5:
        ''' Uses a fixed orientation for part 1-4 '''

        # Orientation of paint brush strokes
        theta = 2 * pi * np.random.rand(1, 1)[0][0]

        # Set vector from center to one end of the stroke.
        delta = np.array([cos(theta), sin(theta)])


    time.time()
    time.clock()
    i = 1
    cntr = np.array([-1, -1])
    while np.any(np.where(canvas == -1)):

        # Indices of unpainted pixels
        unpainted = np.where(canvas == -1)[0:2]

        # Unpainted pixel coordinates x and y
        uy, ux = unpainted

        # Combines them as location of pixel (x, y)
        up = zip(ux, uy)

        unpainted_count = len(up)

        if partNum == 1:
            ''' Performs a simple random sampling '''
            cntr = np.floor(np.random.rand(2,1).flatten()
                            * np.array([sizeIm[1], sizeIm[0]])) + 1
            cntr = np.amin(np.vstack(
                (cntr, np.array([sizeIm[1], sizeIm[0]]))), axis=0)

        else:
            ''' Performs a systematic sampling '''

            # Finding a negative pixel
            # Randomly selects a stroke center from the set of unpainted pixel
            # locations
            randindex = up[(np.floor(
                np.random.rand(1)[0] * len(up)) - 1).astype(int)]

            # Sets and adjusts the center coordinates
            cntr = np.amin(np.vstack(
                (cntr, np.array([sizeIm[1], sizeIm[0]]))), axis=0)

            cntr[0] = randindex[0] + 1
            cntr[1] = randindex[1] + 1

        # Grabs colour from image at center position of the stroke.
        colour = np.reshape(imRGB[cntr[1]-1, cntr[0]-1, :],(3,1))

        # Adds the stroke to the canvas
        nx, ny = (sizeIm[1], sizeIm[0])
        length1, length2 = (halfLen, halfLen)

        if partNum >= 4:
            ''' Uses stroke radius of 1 for steps 4, 5 and 6. '''
            rad = 1

        if partNum >= 5:
            '''
            Computes the orientation based on gradient angles and
            sets half length of stroke to 5 for part 5.
            '''
            delta = np.array([cos(theta[cntr[1] - 1, cntr[0] - 1]  ),
                              sin(theta[cntr[1] - 1, cntr[0] - 1] )])

        if partNum == 6:
            ''' Adds random perturbations to stroke '''
            # Copies the color array to make the calculations on it
            pert = list(colour)

            # Perturbs the color by random amounts from range [-5/255, 5/255]
            for j in range(3):
                pert[j] = (colour[j] + np.random.uniform(-5, 5)/255)

                # Checks if the perturbed color is in range [0, 1]
                if pert[j] < 0 or pert[j] > 1: pert[j] = colour[j]

                # Scales perturbed color by random amount
                # from range [0.95, 1.05]
                pert[j] *= np.random.uniform(0.95, 1.05)  # Intensity

                # Checks if the perturbed color is in range [0, 1]
                if pert[j] < 0 or pert[j] > 1: pert[j] = colour[j]

            # Sets the color array to perturbed array
            colour = list(pert)

            # Perturbs the orientation of stroke by random amount
            # from range [-5, 5] degrees
            for k in range(2):
                pert_theta = pi/36  # 5 degrees = pi/36
                delta[k] += abs((np.random.uniform(-pert_theta, pert_theta)))

        if partNum <= 3:
            ''' Simple fixed length painting '''
            canvas = paintStroke(canvas, x, y, cntr - delta * length2,
                                 cntr + delta * length1, colour, rad)

        else:
            ''' Painting based on endpoints obtained with edge detection '''

            # Calculates the first endpoint.
            # 'flag' denotes whether the center is an edgel or not
            flag, p0 = stoke_clip(canny_img, cntr, delta, halfLen)

            if flag:
                # Use a stroke of length of 0
                canvas = paintStroke(canvas, x, y, cntr, cntr, colour, rad)
            else:
                delta2 = -delta
                # Calculates the second end point, and paints the stroke
                flag2, p1 = stoke_clip(canny_img, cntr, delta2, halfLen)
                canvas = paintStroke(canvas, x, y, p0, p1, colour, rad)

        if i % 250 == 0:
            print "Step: ", i, "Remaining pixel count: ", unpainted_count

        i += 1


    if (i - 1) % 100 != 0:
        print "Step: ", i, "Remaining pixel count: ", 0

    time.time()

    canvas[canvas < 0] = 0.0
    plt.clf()
    plt.axis('off')
    plt.imshow(canvas)
    plt.pause(3)
    # Saves the output images of all steps and
    # canny images of steps 3 to 6 to folder output
    colorImSave('output' + imgLoc[:-4] + str(partNum) + '.png', canvas)
    if partNum >= 3:
        colorImSave('canny' + imgLoc[:-4] + str(partNum) + '.png', canny_img)


if __name__ == "__main__":
    # for i in range(1, 7):
    #     doPart(i, 'orchid.jpg')
    #     print "Part #" + str(i), "is done."
    #     print 50 * "*"

    for i in range(6, 7):
        doPart(i, 'snowmonkey.jpg')
        print "Part #" + str(i), "is done."
        print 50 * "*"